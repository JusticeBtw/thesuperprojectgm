#include <iostream>
#include <math.h>

int main (){
    int x;
    std::cout << "Введите число" << std::endl;
    std::cin >> x;

    int spaces = 0;

    for (int y = x; y > 0; y--){
        for (int p = spaces; p > 0; p--){
            std::cout << " ";
        }
        for (int i = y; i > 0; i--)
            std::cout << "*";
        std::cout << std::endl;
        //spaces = spaces + 1;
        //spaces += 1;
        ++spaces;
        //spaces++;
    } 

    std::cin.get();
    std::cin.get();
}